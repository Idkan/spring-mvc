 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Spring MVC Form</title>
</head>
<body>
	<h3>Spring MVC Form</h3>
	<hr>  
	<h2>Student Information</h2>
	<form:form method = "POST" action = "savePerson">
		<table>
			<tr>
				<td><form:label path="personId">Id</form:label></td>
				<td><form:input path="personId"/></td>
			</tr>
			<tr>
				<td><form:label path="name">Name</form:label></td>
				<td><form:input path="name"/></td>
			</tr>
			<tr>
				<td><form:label path="age">Age</form:label></td>
				<td><form:input path="age"/></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" value="Submit"/>
				</td>
			</tr>
		</table>
	</form:form>
</body>
</html>