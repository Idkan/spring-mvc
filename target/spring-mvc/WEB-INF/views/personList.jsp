<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>   
  
<html>
<head>
<meta charset="ISO-8859-1">
<title>Spring MVC</title>
</head>
<body>
	<h3>Person List</h3>
	<hr>
	<table border="1" width="70%">
		<tr>
			<th>Person Id</th>
			<th>Name</th>
			<th>Age</th>
		</tr>
		<c:forEach var="person" items="${personList}">
			<tr>
				<td align="center">${person.personId}</td>
				<td align="center">${person.name}</td>
				<td align="center">${person.age}</td>
				<td align="center"><a href="editPerson/${person.personId}">Edit</a></td>
				<td align="center"><a href="deletePerson/${person.personId}">Delete</a></td>
			</tr>
		</c:forEach>
	</table>
	<a href="person"> Add New Person</a>
</body>
</html>