package com.softtek.academy.spring.mvc.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Person {

	@Id
	@Column(name = "id")
	private Integer personId;
	
	@Column(name = "age")
	private Integer age;
	
	@Column(name = "name", length = 50)
	private String name;
	
	public Person () {
		
	}
	
	public Person(Integer personId, Integer age, String name) {
		super();
		this.personId = personId;
		this.age = age;
		this.name = name;
	}

	public Integer getPersonId() {
		return personId;
	}
	public void setPersonId(Integer personId) {
		this.personId = personId;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


}

