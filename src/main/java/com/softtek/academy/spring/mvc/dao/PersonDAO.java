package com.softtek.academy.spring.mvc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.softtek.academy.spring.mvc.beans.Person;

public class PersonDAO {

	private JdbcTemplate jdbcTemplate;
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public int savePerson(Person person) {
		String query="INSERT INTO student VALUES('" + person.getName() + "','" + person.getAge() + "','" + person.getPersonId() + "')";
		return jdbcTemplate.update(query);
	}
	
	public List<Person> getPersonList() {
		return jdbcTemplate.query("SELECT * FROM student", new RowMapper<Person>() {
			public Person mapRow(ResultSet rs, int row) throws SQLException {
				Person person = new Person();
				person.setName(rs.getString(1));
				person.setAge(rs.getInt(2));
				person.setPersonId(rs.getInt(3));
				return person;
			}
		});
	}
	
	public Person getPersonById(int idPerson) {
		String query = "SELECT * FROM student WHERE id = ?";
		return jdbcTemplate.queryForObject(query, new Object[] {idPerson}, new BeanPropertyRowMapper<Person>(Person.class));
	}
	
	public int updatePerson(Person person) {
		String query = "UPDATE student set name = '" + person.getName() + "', age = " + 
				person.getAge() + " WHERE id = " + person.getPersonId()  + "";  
	    return jdbcTemplate.update(query);   
	}
	
	public int deletePerson(int personId) {
		String query = "DELETE FROM  student WHERE id = " + personId + "";
		return jdbcTemplate.update(query);
	}
	
}
