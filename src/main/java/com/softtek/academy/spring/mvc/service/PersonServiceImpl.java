package com.softtek.academy.spring.mvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.softtek.academy.spring.mvc.beans.Person;
import com.softtek.academy.spring.mvc.dao.PersonDAOjpa;

@Service("personService")
@Transactional
public class PersonServiceImpl implements PersonService {

	@Autowired
	@Qualifier("personRepository")
	private PersonDAOjpa personDao;

	public PersonServiceImpl(PersonDAOjpa personDao) {
		this.personDao = personDao;
	}
	
	@Override
	public void savePerson(Person person) {
		personDao.savePerson(person);
	}

	@Override
	public List<Person> getPersonList() {
		return personDao.getPersonList();
	}

	@Override
	public Person getPersonById(int idPerson) {
		return personDao.getPersonById(idPerson);
	}

	@Override
	public void updatePerson(Person person) {
		personDao.updatePerson(person);
	}

	@Override
	public void deletePerson(int idPerson) {
		personDao.deletePerson(idPerson);
	}

}
