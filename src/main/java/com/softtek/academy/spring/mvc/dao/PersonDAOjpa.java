package com.softtek.academy.spring.mvc.dao;

import java.util.List;

import com.softtek.academy.spring.mvc.beans.Person;

public interface PersonDAOjpa {

	void savePerson(Person person);

	List<Person> getPersonList();

	Person getPersonById(int idPerson);

	void updatePerson(Person person);

	void deletePerson(int idPerson);

}
