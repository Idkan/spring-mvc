package com.softtek.academy.spring.mvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.spring.mvc.beans.Person;
import com.softtek.academy.spring.mvc.service.PersonService;

@Controller
public class PersonController {
	
	@Autowired
	@Qualifier("personService")
	PersonService personService;
	
	
	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	@RequestMapping(value = "/person", method = RequestMethod.GET)
	public ModelAndView student() {
		return new ModelAndView("person", "command", new Person());
	}
	
	@RequestMapping(value = "/savePerson", method = RequestMethod.POST)
    public String addPerson(@ModelAttribute("person")Person person, ModelMap model) {
        model.addAttribute("name", person.getName());
        model.addAttribute("age", person.getAge());
        model.addAttribute("personId", person.getPersonId());
        personService.savePerson(person);
        return "redirect:/personList";
    }
	
	@RequestMapping(value = "/personList")
	public String showPerson(Model model) {
		List<Person> personList = personService.getPersonList();
		model.addAttribute("personList",personList);
		return "personList";
	}
	
	@RequestMapping(value = "/editPerson/{idPerson}")
	public String editPerson(@PathVariable int idPerson, Model model) {
		Person person = personService.getPersonById(idPerson);
		model.addAttribute("command", person);
		return "editPersonForm";
		
	}
	
	@RequestMapping(value = "/editPersonSave", method = RequestMethod.POST)
	public String editPersonSave(@ModelAttribute("person")Person person) {
        personService.updatePerson(person);
		return "redirect:/personList";
	}
	
	@RequestMapping(value = "/deletePerson/{idPerson}", method = RequestMethod.GET)
	public String deletePerson(@PathVariable int idPerson) {
		personService.deletePerson(idPerson);
		return "redirect:/personList";
	}

}
