package com.softtek.academy.spring.mvc.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.softtek.academy.spring.mvc.beans.Person;

@Repository("personRepository")
public class PersonRepository implements PersonDAOjpa {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void savePerson(Person person) {
		Person p = new Person();
		p.setPersonId(person.getPersonId());
		p.setName(person.getName());
		p.setAge(person.getAge());
		entityManager.persist(p);
	}

	@Override
	public List<Person> getPersonList() {
		return entityManager.createQuery("Select p from Person p", Person.class).getResultList();
	}

	@Override
	public Person getPersonById(int idPerson) {
		return entityManager.find(Person.class, idPerson);
	}

	@Override
	public void updatePerson(Person person) {
		Person p = entityManager.find(Person.class, person.getPersonId());
		if (person != null) {
			p.setName(person.getName());
			p.setAge(person.getAge());
		}
	}

	@Override
	public void deletePerson(int idPerson) {
		Person person = entityManager.find(Person.class, idPerson);
		if (person != null) {
			entityManager.remove(person);
		}
	}

}
