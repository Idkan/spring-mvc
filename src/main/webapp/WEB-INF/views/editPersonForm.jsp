 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Spring MVC Form</title>
</head>
<body>
	<h3>Spring MVC Form</h3>
	<hr>  
	<h2>Edit Student Information</h2>
	<form:form method = "POST" action = "/spring-mvc/editPersonSave">
		<table>
			<tr>
				<td></td>
				<td><form:hidden path="personId"/></td>
			</tr>
			<tr>
				<td>Name</td>
				<td><form:input path="name"/></td>
			</tr>
			<tr>
				<td>Age</td>
				<td><form:input path="age"/></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" value="Submit"/>
				</td>
			</tr>
		</table>
	</form:form>
</body>
</html>