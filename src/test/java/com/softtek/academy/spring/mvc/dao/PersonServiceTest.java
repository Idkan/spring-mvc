package com.softtek.academy.spring.mvc.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.softtek.academy.spring.mvc.beans.Person;
import com.softtek.academy.spring.mvc.service.PersonService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/main/webapp/WEB-INF/spring-servlet.xml")
@WebAppConfiguration
public class PersonServiceTest {

	@Autowired
	private PersonService personService;
	
	@Test
	public void getPersonIdTest( ) {

		// Setup
		List<Person> person = personService.getPersonList();
		int expected = 1;
		
		// Execute
		int actual = person.get(0).getPersonId();
		
		// Validate
		assertNotNull(person);
		assertSame(expected, actual);
		
	}
	
	@Test
	public void getPersonById() {
		
		// Setup
		int idExpected = 1;
		int ageExpected = 11;
		
		Person person = personService.getPersonById(1);
		
		// Execute
		int actualId = person.getPersonId();
		int actualAge = person.getAge();

			
		// Validate
		
		assertNotNull(person);
		assertSame("Input id doesn't match", idExpected, actualId);
		assertSame("Input age doesn't match", ageExpected, actualAge);
	}
}
